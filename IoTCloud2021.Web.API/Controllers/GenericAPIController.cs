﻿using IoTCloud2021.COMMON.Entidades;
using IoTCloud2021.COMMON.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace IoTCloud2021.Web.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public abstract class GenericAPIController<T> : ControllerBase where T:BaseDTO
    {

        protected IGenericManager<T> manager;
        public GenericAPIController(IGenericManager<T> manager)
        {
            this.manager = manager;
        }

        //[HttpGet("ObtenerPagina")]
        //public ActionResult<IEnumerable<T>> ObtenerPagina(int apartirDeRegistroNum, int registrosAObtener, string ordenadarPorCampo = "", bool ascendente = true)
        //{
        //    try
        //    {

        //        var r = manager.(apartirDeRegistroNum, registrosAObtener, ordenadarPorCampo, ascendente);
        //        if (r != null)
        //        {
        //            return Ok(r);
        //        }
        //        else
        //        {
        //            return BadRequest(manager.Error);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return BadRequest(ex.Message);
        //    }
        //}


        [HttpGet]

        public ActionResult<IEnumerable<T>> Get()

        {
            try
            {
                var r = manager.ObtenerTodos;

                if (r != null)
                {
                    return Ok(r);
                }
                else
                {
                    return BadRequest(manager.Error);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // GET api/<GenericApiController>/5
        [HttpGet("{id}")]
        public ActionResult<T> Get(string id)
        {
            try
            {
                var r = manager.BuscarPorId(id);
                if (r != null)
                {
                    return Ok(r);
                }
                else
                {
                    return BadRequest(manager.Error);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // POST api/<GenericApiController>
        [HttpPost]
        public ActionResult<T> Post([FromBody] T value)
        {
            try
            {
                var r = manager.Insertar(value);
                if (r != null)
                {
                    return Ok(r);
                }
                else
                {
                    return BadRequest(manager.Error);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // PUT api/<GenericApiController>/5
        [HttpPut]
        public ActionResult<T> Put([FromBody] T value)
        {
            try
            {
                var r = manager.Actualizar(value);
                if (r != null)
                {
                    return Ok(r);
                }
                else
                {
                    return BadRequest(manager.Error);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        // DELETE api/<GenericApiController>/5
        [HttpDelete("{id}")]
        public ActionResult<bool> Delete(string id)
        {
            try
            {
                var r = manager.Eliminar(id);
                if (r)
                {
                    return Ok(r);
                }
                else
                {
                    return BadRequest(manager.Error);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
