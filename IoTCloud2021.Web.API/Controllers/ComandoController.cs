﻿using IoTCloud2021.BIZ;
using IoTCloud2021.COMMON.Entidades;
using IoTCloud2021.COMMON.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoTCloud2021.Web.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ComandoController : GenericAPIController<Comando>
    {
        public ComandoController() : base(FabricManager.Comando)
        {
        }
    }
}
