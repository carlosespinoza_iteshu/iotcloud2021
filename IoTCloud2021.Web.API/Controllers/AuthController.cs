﻿using IoTCloud2021.BIZ;
using IoTCloud2021.COMMON.Interfaces;
using IoTCloud2021.COMMON.Modelos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace IoTCloud2021.Web.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IConfiguration configuration;
        public AuthController(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
        [HttpPost("Login")]
        public ActionResult<TokenModel> Login([FromBody] LoginModel model)
        {
            try
            {
                if (model != null)
                {
                    var u = FabricManager.UsuarioManager.Login(model);
                    if (u != null)
                    {
                        var secretKey = configuration.GetValue<string>("SecretKey");
                        var key = Encoding.ASCII.GetBytes(secretKey);
                        List<Claim> claims = new List<Claim>()
                        {
                            new Claim(ClaimTypes.NameIdentifier,u.Id.ToString()),
                            new Claim(ClaimTypes.Name, u.Nombre),
                            new Claim(ClaimTypes.Email, u.Email)
                        };
                        var tokenDescriptor = new SecurityTokenDescriptor
                        {
                            Subject = new ClaimsIdentity(claims),
                            Expires = DateTime.UtcNow.AddDays(1),
                            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                        };
                        var tokenHandler = new JwtSecurityTokenHandler();
                        var token = tokenHandler.CreateToken(tokenDescriptor);
                        return Ok(new TokenModel()
                        {
                            Token = tokenHandler.WriteToken(token),
                            FechaHoraCreacion = DateTime.Now,
                            FechaHoraTermino = tokenDescriptor.Expires.Value,
                            Email = u.Email,
                            IdUsuario = u.Id,
                            Nombre = u.Nombre + " " + u.Apellidos,
                            UrlFoto = u.URLFoto
                        });
                    }
                    else
                    {
                        return Forbid();
                    }
                }
                else
                {
                    return Forbid();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
