﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoTCloud2021.Tools
{
    public static class Herramientas
    {
        public static string GenerarTopicoMQTT()
        {
            return Guid.NewGuid().ToString().Substring(5, 10);
        }
    }
}
