/*
 Name:		ESP8266_01.ino
 Created:	9/19/2021 10:03:56 AM
 Author:	Carlos Espinoza (cespinoza@iteshu.edu.mx)
*/

#include <ESP8266HTTPClient.h>
#include <DHTesp.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

//Definición de pines
const int pinPhotoResistencia = A0;
const int pinBuzzer = D2;
const int pinLed = D0;
const int pinPulsador = D7;
const int pinDHT = D4;

const char* idDispositivo = "5c254d3ced0dc527546ac25e";
const char* ssid = "INFINITUM308C_2.4";
const char* password = "5nNUF2Rdhb";

const char* mqttServer = "200.79.179.167";
const int mqttPort = 1884;

char* server = "http://40.119.56.63/php-crud-api/api.php/records/datosiot";
char* userIoTCloud = "cespinoza@iteshu.edu.mx";
char* passIoTCloud = "1234567890";
WiFiClient espClient;
PubSubClient client(espClient);
DHTesp dht;
HTTPClient http;
//Variables de apoyo
int buzzer = 0;
String Data = "";
int segundos = 0;
int tiempo =10 * 2;
int pulsos = 0;
int eL = 0;
int eB = 0;
int anguloMotor = 0;

void setup() {
    pinMode(pinPhotoResistencia, INPUT);
    pinMode(pinBuzzer, OUTPUT);
    pinMode(pinLed, OUTPUT);
    pinMode(pinPulsador, INPUT);
    dht.setup(pinDHT, DHTesp::DHT11);
    Serial.begin(9600);
    IniciarConexionWiFi();
    //ConectarMQTT();
    delay(2000);
}

void ConectarMQTT() {
    client.setServer(mqttServer, mqttPort);
    client.setCallback(callback);

    while (!client.connected()) {
        Serial.println("Conectando a MQTT...");

        if (client.connect("NodeMCU")) {

            Serial.println("Conectado!!! :)");

        }
        else {
            Serial.print("Conexion fallada con estado: ");
            Serial.print(client.state());
            Serial.print(" esperando 2 segs.");
            delay(2000);
        }
        String trama = String("iotcloud_esp8266/" + String(idDispositivo));
        int len = trama.length();
        char tema[len];
        trama.toCharArray(tema, len + 1);
        client.subscribe(tema);
        PublicarMQTT("test", "Dispositivo NodeMCU Carlos OnLine");
    }
}
void callback(char* topic, byte* payload, unsigned int length) {

    Serial.print("Mensaje recibido en: ");
    Serial.println(topic);

    Serial.print("Mensaje:");
    String command = "";
    for (int i = 0; i < length; i++) {
        Serial.print((char)payload[i]);
        command = String(command + (char)payload[i]);
    }
    if (command == "B0") {
        Sonar(false);
    }
    if (command == "B1") {
        Sonar(true);
    }
    if (command == "L0") {
        Led(false);
    }
    if (command == "L1") {
        Led(true);
    }
    if (command == "AS") {
        MandaDatosSensores();
    }
    if (command == "S") {
        String data = String(String(idDispositivo) + ";Estado;" + String(eL) + ";" + String(eB) + ";" + String(anguloMotor));
        PublicarMQTT("test", data);
    }
    
}
void IniciarConexionWiFi() {

    WiFi.begin(ssid, password);
    Serial.print("Conectando SSID: ");
    Serial.println(ssid);
    while (WiFi.status() != WL_CONNECTED)  //Intento de conexion a la red ssid
    {
        Serial.print(".");
        delay(500);
    }
    Serial.print("Conexion Establecida con ");
    Serial.println(ssid);
    Serial.print(" con la IP:");
    Serial.println(WiFi.localIP());
}



float LeePhotoResistencia() {
    float lectura = analogRead(pinPhotoResistencia);
    Serial.print("Luminosidad: ");
    Serial.println(lectura);
    return lectura;
}

void Sonar(bool sonar) {
    Data = String(String(idDispositivo) + ";Buzzer;");
    if (sonar) {
        digitalWrite(pinBuzzer, HIGH);
        eB = 1;
        Data += String("B1");
    }
    else {
        digitalWrite(pinBuzzer, LOW);
        eB = 0;
        Data += String("B0");
    }
    PublicarMQTT("test", Data);
}

void Led(bool encender) {
    Data = String(String(idDispositivo) + ";Led;");
    if (encender) {
        digitalWrite(pinLed, HIGH);
        eL = 1;
        Data += String("L1");
    }
    else {
        digitalWrite(pinLed, LOW);
        eL = 0;
        Data += String("L0");
    }
    PublicarMQTT("test", Data);
}

bool DetectarPulso() {
    if (digitalRead(pinPulsador) == HIGH) {
        Serial.println("Pulso");
        return true;
    }
    else {
        return false;
    }
}

float LeerTemperatura() {
    float h = dht.getTemperature();
    Serial.print("Humedad: ");
    Serial.println(h);
    return h;
}

float LeerHumedad() {
    float t = dht.getHumidity();
    Serial.print("Temperatura: ");
    Serial.println(t);
    return t;
}


void PublicarMQTT(char* topic, String data) {
    Serial.println(data);
    int len = data.length();
    char datos[len];
    data.toCharArray(datos, len + 1);
    String trama = String("iotcloud_esp8266/" + String(idDispositivo));
    len = trama.length();
    char tema[len];
    trama.toCharArray(tema, len + 1);
    client.publish(tema, datos);
}
void MandaDatosSensores() {
    Data = String(String(idDispositivo) + ";Sensores;" + String(LeerTemperatura()) + ";" + String(LeerHumedad()) + ";" + String(LeePhotoResistencia()));
    PublicarMQTT("test", Data);
}

void loop() {
    client.loop();

    if (segundos == tiempo) {
        segundos = 0;
        LeerHumedad();
        LeerTemperatura();
        LeePhotoResistencia();
        //MandaDatosSensores();
    }
    DetectarPulso();

    segundos++;
    delay(100);


}
