﻿using FluentValidation.Results;
using IoTCloud2021.COMMON.Entidades;
using IoTCloud2021.COMMON.Interfaces;
using IoTCloud2021.COMMON.Validadores;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace IoTCloud2021.DAL
{
    public class GenericRepository<T>:IGenericRepository<T> where T:BaseDTO
    {
        private bool resultado;
        private readonly IMongoDatabase db;
        private readonly GenericValidator<T> validator;
        private ValidationResult resultV;
        public GenericRepository(IMongoDatabase db, GenericValidator<T> validator)
        {
            this.db = db;
            this.validator = validator;
        }

        private IMongoCollection<T> Collection() => db.GetCollection<T>(typeof(T).Name);

        public string Error { get; private set; }

        public IEnumerable<T> Read
        {
            get
            {
                var r = Collection().AsQueryable();
                return r;
            }
        }

        public int Count => Read.Count();

        public IEnumerable<T> AddMultiple(IEnumerable<T> entidades)
        {
            List<T> noInsertadas = new List<T>();
            string errores = "";
            foreach (var entidad in entidades)
            {
                if (Create(entidad) == null)
                {
                    noInsertadas.Add(entidad);
                    errores += Error;
                }
            }
            Error = errores;
            return noInsertadas;
        }

        public T Create(T entidad)
        {
            entidad.Id = Guid.NewGuid().ToString();
            entidad.FechaHora = DateTime.Now;
            resultV = validator.Validate(entidad);
            if (resultV.IsValid)
            {
                try
                {
                    Collection().InsertOne(entidad);
                    Error = "";
                    resultado = true;
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                    resultado = false;
                }
            }
            else
            {
                Error = $"Datos inválidos: ";
                foreach (var error in resultV.Errors)
                {
                    Error += "\n" + error.ErrorMessage;
                }
                resultado = false;
            }
            return resultado ? entidad : null;
        }

        public bool Delete(string id)
        {
            try
            {
                int r = (int)Collection().DeleteOne(e => e.Id == id).DeletedCount;
                resultado = r == 1;
                Error = resultado ? "" : $"Se eliminaron {r} elementos";
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                resultado = false;
            }
            return resultado;
        }

        public IEnumerable<string> DeleteMultiple(IEnumerable<string> ids)
        {
            List<string> noInsertados = new List<string>();
            string errores = "";
            foreach (var id in ids)
            {
                if (!Delete(id))
                {
                    noInsertados.Add(id);
                    errores += Error;
                }
            }
            Error = errores;
            return noInsertados;
        }

        public IEnumerable<T> Query(Expression<Func<T, bool>> predicado)
        {
            var r = Read.Where(predicado.Compile());
            return r;
        }

        public T SearchById(string id)
        {
            var r = Query(e => e.Id == id).SingleOrDefault();
            return r;
        }

        public T Update(T entidad)
        {
            entidad.FechaHora = DateTime.Now.AddHours(-6);
            resultV = validator.Validate(entidad);
            if (resultV.IsValid)
            {
                try
                {
                    int r = (int)Collection().ReplaceOne(e => e.Id == entidad.Id, entidad).ModifiedCount;
                    resultado = r == 1;
                    Error = resultado ? "" : $"Se modificaron {r} elementos";
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                    resultado = false;
                }
            }
            else
            {
                Error = $"Datos inválidos: ";
                foreach (var error in resultV.Errors)
                {
                    Error += "\n" + error.ErrorMessage;
                }
                resultado = false;
            }
            return resultado ? entidad : null;
        }

        public IEnumerable<T> ReadPage(int apartirDeRegistroNum, int registrosAObtener, bool ascendente = true)
        {
            IEnumerable<T> datos = ascendente ? Read.OrderBy(d => d.FechaHora) : Read.OrderByDescending(d => d.FechaHora);
            return datos.Skip(apartirDeRegistroNum).Take(registrosAObtener);
        }
    }
}
