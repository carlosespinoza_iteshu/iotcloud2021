﻿using IoTCloud2021.COMMON.Entidades;
using IoTCloud2021.COMMON.Interfaces;
using IoTCloud2021.COMMON.Validadores;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoTCloud2021.DAL
{
    public static class FabricRepository
    {
#if DEBUG
        static IMongoDatabase db=new MongoClient(new MongoUrl(DALResources.MongoDBConectionStringDEV)).GetDatabase(DALResources.MongoDBNameDEV);
#else
        static IMongoDatabase db=new MongoClient(new MongoUrl(DALResources.MongoDBConectionString)).GetDatabase(DALResources.MongoDBName);
#endif
        public static IGenericRepository<Actuador> ActuadorRepository = new GenericRepository<Actuador>(db, new ActuadorValidator());
        public static IGenericRepository<Comando> ComandoRepository = new GenericRepository<Comando>(db, new ComandoValidator());
        public static IGenericRepository<EstadoActuador> EstadoActuadorRepository = new GenericRepository<EstadoActuador>(db, new EstadoActuadorValidator());
        public static IGenericRepository<Lectura> LecturaRepository = new GenericRepository<Lectura>(db, new LecturaValidator());
        public static IGenericRepository<Proyecto> ProyectoRepository = new GenericRepository<Proyecto>(db, new ProyectoValidator());
        public static IGenericRepository<Sensor> SensorRepository = new GenericRepository<Sensor>(db, new SensorValidator());
        public static IGenericRepository<Usuario> UsuarioRepository = new GenericRepository<Usuario>(db, new UsuarioValidator());
    }
}
