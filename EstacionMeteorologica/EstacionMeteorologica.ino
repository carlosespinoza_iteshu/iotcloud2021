/*
 Name:		EstacionMeteorologica.ino
 Created:	9/19/2021 1:30:44 PM
 Author:	espin
*/

#include <ESP8266WiFi.h>
#include <WiFiManager.h>

WiFiManager wifi;
void setup() {
	Serial.begin(9600);
	Serial.println("Esperando configuración WiFi");
	wifi.autoConnect("Estacion Meteorologica");
	Serial.println("Conectada");
}

void loop() {
  
}
