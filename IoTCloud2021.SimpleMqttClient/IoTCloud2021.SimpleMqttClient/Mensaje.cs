﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoTCloud2021.SimpleMqttClient
{

    public enum TipoMensaje
    {
        Entrada,
        Salida
    }

    public class Mensaje
    {
        public TipoMensaje Tipo { get; set; }
        public string Topico { get; set; }
        public string Contenido { get; set; }
        public string Imagen { get; private set; }
        public DateTime FehaHora { get; private set; }
        public Mensaje(TipoMensaje tipo, string topico, string contenido)
        {
            Tipo = tipo;
            Topico = topico;
            Contenido = contenido;
            if(tipo== TipoMensaje.Entrada)
            {
                Imagen = "https://drive.google.com/file/d/1y0rPfOqzUtOrh61XegM6ilgFRckbDHne/view?usp=sharing";
            }
            else
            {
                Imagen = "https://drive.google.com/file/d/1y7zl3lXkz4Od09SE-24Dhaom3vvHz4Qj/view?usp=sharing";
            }
            FehaHora = DateTime.Now;
        }
        public override string ToString()
        {
            if (Tipo == TipoMensaje.Entrada)
                return $"E:{Topico}=>{Contenido}";
            else
                return $"S:{Topico}=>{Contenido}";
        }
    }
}
