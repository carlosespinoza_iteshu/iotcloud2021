﻿using OpenNETCF.MQTT;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace IoTCloud2021.SimpleMqttClient
{
    public partial class MainPage : ContentPage
    {
        MQTTClient clientMqtt;
        bool nuevoMensaje = false;
        List<Mensaje> mensajes;
        
        public MainPage()
        {
            InitializeComponent();
            mensajes = new List<Mensaje>();
        }

        private void btnConectar_Clicked(object sender, EventArgs e)
        {
            clientMqtt = new MQTTClient(entServer.Text, int.Parse(entPuerto.Text));
            clientMqtt.MessageReceived += ClientMqtt_MessageReceived;
            DisplayAlert("SimpleMqttClient", "Espere a que se conecte...", "Ok");
            if (string.IsNullOrEmpty(entUser.Text))
            {
                clientMqtt.Connect(Guid.NewGuid().ToString().Substring(0, 10));
            }
            else
            {
                clientMqtt.Connect(Guid.NewGuid().ToString(), entUser.Text, entPassword.Text);
            }
            while (!clientMqtt.IsConnected)
            {
                Thread.Sleep(1000);
            }
            DisplayAlert("SimpleMqttClient", "Conectado", "Ok");
            btnConectar.IsEnabled = false;
            Device.StartTimer(TimeSpan.FromMilliseconds(200),()=>
            {
                if (nuevoMensaje)
                {
                    nuevoMensaje = false;
                    lstMensajes.ItemsSource = null;
                    lstMensajes.ItemsSource = mensajes.OrderByDescending(m => m.FehaHora);
                }
                return true;
            });
        }

        private void ClientMqtt_MessageReceived(string topic, QoS qos, byte[] payload)
        {
            mensajes.Add(new Mensaje(TipoMensaje.Entrada, topic, Encoding.UTF8.GetString(payload)));
            nuevoMensaje = true;
        }


        private void btnSuscribirse_Clicked(object sender, EventArgs e)
        {
            if (clientMqtt.IsConnected)
            {
                clientMqtt.Subscriptions.Add(new Subscription(entTopic.Text));
                DisplayAlert("SimpleMqttClient", "Suscrito", "Ok");
                entTopic.Text = "";
            }
            else
            {
                DisplayAlert("SimpleMqttClient", "Primero conectate", "Ok");
            }
        }

        private void btnEnviar_Clicked(object sender, EventArgs e)
        {
            if (clientMqtt.IsConnected)
            {
                if (!string.IsNullOrEmpty(entTopic.Text))
                {
                    if (!string.IsNullOrEmpty(entMensaje.Text))
                    {
                        clientMqtt.Publish(entTopic.Text, entMensaje.Text, QoS.FireAndForget, false);
                        mensajes.Add(new Mensaje(TipoMensaje.Salida, entTopic.Text, entMensaje.Text));
                        nuevoMensaje = true;
                    }
                    else
                    {
                        DisplayAlert("SimpleMqttClient", "Escribe el mensaje", "Ok");
                    }
                }
                else
                {
                    DisplayAlert("SimpleMqttClient", "Escribe el tópico al cual enviar el mensaje", "Ok");
                }
            }
            else
            {
                DisplayAlert("SimpleMqttClient", "Primero conectate", "Ok");
            }


        }
    }
}
