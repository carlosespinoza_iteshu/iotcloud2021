﻿using IoTCloud2021.COMMON.Interfaces;
using IoTCloud2021.DAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoTCloud2021.BIZ
{
    public static class FabricManager
    {
        public static IActuadorManager ActuadorManager => new ActuadorManager(FabricRepository.ActuadorRepository);
        public static IComandoManager Comando => new ComandoManager(FabricRepository.ComandoRepository);
        public static IEstadoActuadorManager EstadoActuadorManager => new EstadoActuadorManager(FabricRepository.EstadoActuadorRepository);
        public static ILecturaManager LecturaManager => new LecturaManager(FabricRepository.LecturaRepository);
        public static IProyectoManager ProyectoManager => new ProyectoManager(FabricRepository.ProyectoRepository);
        public static ISensorManager SensorManager => new SensorManager(FabricRepository.SensorRepository);
        public static IUsuarioManager UsuarioManager => new UsuarioManager(FabricRepository.UsuarioRepository);
    }
}
