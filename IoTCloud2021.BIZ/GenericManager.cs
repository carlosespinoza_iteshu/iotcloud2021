﻿using IoTCloud2021.COMMON.Entidades;
using IoTCloud2021.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoTCloud2021.BIZ
{
    public abstract class GenericManager<T> : IGenericManager<T> where T : BaseDTO
    {
        protected IGenericRepository<T> repository;
        public GenericManager(IGenericRepository<T> genericRepository)
        {
            repository = genericRepository;
        }

        public string Error => repository.Error;

        public IEnumerable<T> ObtenerTodos => repository.Read;

        public int TotalDeElementos => repository.Count;

        public T Actualizar(T entidad) => repository.Update(entidad);

        public IEnumerable<T> AgregaVarias(IEnumerable<T> entidades) => repository.AddMultiple(entidades);

        public T BuscarPorId(string id) => repository.SearchById(id);

        public bool Eliminar(string id)
        {

            if (repository.Delete(id))
            {
                switch (typeof(T).Name)
                {
                    case "Actuador":
                        FabricManager.EstadoActuadorManager.EliminarEstadosDeActuador(id);
                        break;
                    case "Proyecto":
                        FabricManager.SensorManager.EliminarSensoresDeProyecto(id);
                        FabricManager.ActuadorManager.EliminarActuadoresDeProyecto(id);
                        FabricManager.Comando.EliminarComandoDeProyecto(id);
                        break;
                    case "Sensor":
                        FabricManager.LecturaManager.EliminarLecturasDeSensor(id);
                        break;
                    case "Usuario":
                        FabricManager.ProyectoManager.EliminarProyectosDeUsuario(id);
                        break;
                }
                return true;
            }
            else
            {
                return false;
            }

        }

        public IEnumerable<string> EliminaVarias(IEnumerable<string> ids) => repository.DeleteMultiple(ids);

        public T Insertar(T entidad) => repository.Create(entidad);

        public IEnumerable<T> ObtenerTodosPaginado(int apartirDeRegistroNum, int registrosAObtener, bool ascendente = true)
        {
            return repository.ReadPage(apartirDeRegistroNum, registrosAObtener, ascendente);
        }
    }
}
