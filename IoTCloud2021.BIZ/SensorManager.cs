﻿using IoTCloud2021.COMMON.Entidades;
using IoTCloud2021.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IoTCloud2021.BIZ
{
    public class SensorManager : GenericManager<Sensor>, ISensorManager
    {
        public SensorManager(IGenericRepository<Sensor> genericRepository) : base(genericRepository)
        {
        }

        public void EliminarSensoresDeProyecto(string idProyecto)
        {
            var datos = repository.Query(c => c.IdProyecto == idProyecto).ToList();
            List<string> ids = new List<string>();
            foreach (var item in datos)
            {
                ids.Add(item.Id);
            }
            repository.DeleteMultiple(ids);
        }

        public void EliminarSensoresDeUsuario(string idUsuario)
        {
            var datos = repository.Query(c => c.IdUsuario == idUsuario).ToList();
            List<string> ids = new List<string>();
            foreach (var item in datos)
            {
                ids.Add(item.Id);
            }
            repository.DeleteMultiple(ids);
        }

        public IEnumerable<Sensor> SensoresDeProyecto(string idProyecto)
        {
            return repository.Query(s => s.IdProyecto == idProyecto);
        }

        public IEnumerable<Sensor> SensoresDeUsuario(string idUsuario)
        {
            return repository.Query(s => s.IdUsuario == idUsuario);
        }
    }
}
