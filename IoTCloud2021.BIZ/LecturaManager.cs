﻿using IoTCloud2021.COMMON.Entidades;
using IoTCloud2021.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IoTCloud2021.BIZ
{
    public class LecturaManager : GenericManager<Lectura>, ILecturaManager
    {
        public LecturaManager(IGenericRepository<Lectura> genericRepository) : base(genericRepository)
        {
        }

        public void EliminarLecturasDeProyecto(string idProyecto)
        {
            var datos = repository.Query(c => c.IdProyecto == idProyecto).ToList();
            List<string> ids = new List<string>();
            foreach (var item in datos)
            {
                ids.Add(item.Id);
            }
            repository.DeleteMultiple(ids);
        }

        public void EliminarLecturasDeSensor(string idSensor)
        {
            var datos = repository.Query(c => c.IdSensor == idSensor).ToList();
            List<string> ids = new List<string>();
            foreach (var item in datos)
            {
                ids.Add(item.Id);
            }
            repository.DeleteMultiple(ids);
        }

        public void EliminarLecturasDeUsuario(string idUsuario)
        {
            var datos = repository.Query(c => c.IdUsuario == idUsuario).ToList();
            List<string> ids = new List<string>();
            foreach (var item in datos)
            {
                ids.Add(item.Id);
            }
            repository.DeleteMultiple(ids);
        }

        public IEnumerable<Lectura> LecturasDeSensor(string idSensor)
        {
            return repository.Query(l => l.IdSensor == idSensor);
        }
    }
}
