﻿using IoTCloud2021.COMMON.Entidades;
using IoTCloud2021.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IoTCloud2021.BIZ
{
    public class ActuadorManager : GenericManager<Actuador>, IActuadorManager
    {
        public ActuadorManager(IGenericRepository<Actuador> genericRepository) : base(genericRepository)
        {
        }

        public IEnumerable<Actuador> ActuadoresDeUsuario(string idUsuario)
        {
            return repository.Query(a => a.IdUsuario == idUsuario);
        }

        public void EliminarActuadoresDeProyecto(string idProyecto)
        {
            List<Actuador> actuadores = repository.Query(p => p.IdProyecto == idProyecto).ToList();
            List<string> ids = new List<string>();
            foreach (var item in actuadores)
            {
                ids.Add(item.Id);
            }
            repository.DeleteMultiple(ids);
        }

        public void EliminarActuadoresDeUsuario(string idUsuario)
        {
            List<Actuador> actuadores = repository.Query(p => p.IdUsuario == idUsuario).ToList();
            List<string> ids = new List<string>();
            foreach (var item in actuadores)
            {
                ids.Add(item.Id);
            }
            repository.DeleteMultiple(ids);
        }
    }
}
