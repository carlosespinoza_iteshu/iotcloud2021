﻿using IoTCloud2021.COMMON.Entidades;
using IoTCloud2021.COMMON.Interfaces;
using IoTCloud2021.COMMON.Modelos;
using IoTCloud2021.COMMON.Validadores.Modelos;
using IoTCloud2021.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IoTCloud2021.BIZ
{
    public class UsuarioManager : GenericManager<Usuario>, IUsuarioManager
    {
        public UsuarioManager(IGenericRepository<Usuario> genericRepository) : base(genericRepository)
        {
        }

        public bool CrearCuenta(NuevaCuentaModel model, out string resultado)
        {
            NuevaCuentaModelValidator validator = new NuevaCuentaModelValidator();
            var result = validator.Validate(model);
            if (result.IsValid)
            {
                Usuario u = new Usuario()
                {
                    Apellidos = model.Apellido,
                    Email = model.Email,
                    Nombre = model.Nombre,
                    Password = Tools.Crifrado.CalculateMD5Hash(model.Password1),
                    TopicoMQTT = model.Email.Replace("@", "").Replace(".", "")
                };
                try
                {
                    var r=repository.Create(u);
                    if (r != null)
                    {
                        resultado = "Cuenta creada correctamente, ya puedes iniciar sesión...";
                        return true;
                    }
                    else
                    {
                        resultado = repository.Error;
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    resultado = ex.Message;
                    return false;
                }
            }
            else
            {
                resultado = "Existen errores de validación: \n";
                foreach (var item in result.Errors)
                {
                    resultado += item.ErrorMessage + "\n";
                }
                return false;
            }
            
        }

        public Usuario Login(LoginModel model)
        {
            try
            {
                return repository.Query(u => u.Email == model.Email && u.Password == Crifrado.CalculateMD5Hash(model.Password)).SingleOrDefault();
            }
            catch (Exception)
            {
                return null;
            }
            
        }
    }
}
