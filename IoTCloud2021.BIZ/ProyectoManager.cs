﻿using IoTCloud2021.COMMON.Entidades;
using IoTCloud2021.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IoTCloud2021.BIZ
{
    public class ProyectoManager : GenericManager<Proyecto>, IProyectoManager
    {
        public ProyectoManager(IGenericRepository<Proyecto> genericRepository) : base(genericRepository)
        {
        }

        public void EliminarProyectosDeUsuario(string idUsuario)
        {
            var datos = repository.Query(c => c.IdUsuario == idUsuario).ToList();
            List<string> ids = new List<string>();
            foreach (var item in datos)
            {
                ids.Add(item.Id);
            }
            repository.DeleteMultiple(ids);
        }

        public IEnumerable<Proyecto> ProyectosDeUsuario(string idUsuario)
        {
            return repository.Query(p => p.IdUsuario == idUsuario);
        }
    }
}
