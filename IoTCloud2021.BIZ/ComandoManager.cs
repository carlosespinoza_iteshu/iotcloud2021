﻿using IoTCloud2021.COMMON.Entidades;
using IoTCloud2021.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IoTCloud2021.BIZ
{
    public class ComandoManager : GenericManager<Comando>, IComandoManager
    {
        public ComandoManager(IGenericRepository<Comando> genericRepository) : base(genericRepository)
        {
        }

        public void EliminarComandoDeProyecto(string idProyecto)
        {
            var datos = repository.Query(c => c.IdProyecto == idProyecto).ToList();
            List<string> ids = new List<string>();
            foreach (var item in datos)
            {
                ids.Add(item.Id);
            }
            repository.DeleteMultiple(ids);
        }

        public void EliminarComandoDeUsuario(string idUsuario)
        {
            var datos = repository.Query(c => c.IdUsuario == idUsuario).ToList();
            List<string> ids = new List<string>();
            foreach (var item in datos)
            {
                ids.Add(item.Id);
            }
            repository.DeleteMultiple(ids);
        }
    }
}
