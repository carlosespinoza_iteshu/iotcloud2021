﻿using IoTCloud2021.COMMON.Entidades;
using IoTCloud2021.COMMON.Enumeraciones;
using IoTCloud2021.COMMON.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoTCloud2021.Web.Models
{
    [Authorize]
    [BindProperties]

    public abstract class GenericModelCRUD<T> : PageModel where T : BaseDTO
    {
        protected readonly IGenericManager<T> manager;
        public bool Exito { get; set; }
        public bool EsError { get; set; }
        public string Mensaje { get; set; }
        public T Item { get; set; }
        public EnumTipoOperacion Operacion { get; set; }
        public IEnumerable<T> Items { get; set; }
        public GenericModelCRUD(IGenericManager<T> manager)
        {
            this.manager = manager;
            Item = (T)Activator.CreateInstance(typeof(T));
        }
        public virtual void OnGet(string id)
        {
            Item = manager.BuscarPorId(id);
        }

        public virtual IActionResult OnPost()
        {
            IActionResult r = null;
            switch (Operacion)
            {
                case EnumTipoOperacion.Insertar:
                    r = Insertar();
                    break;
                case EnumTipoOperacion.Actualizar:
                    r = Editar();
                    break;
                case EnumTipoOperacion.Eliminar:
                    r = Eliminar();
                    break;
                default:
                    break;
            }
            return r;
        }


        private IActionResult Insertar()
        {
            Exito = false;
            try
            {

                var r = manager.Insertar(Item);
                if (r != null)
                {
                    Exito = true;
                    Mensaje = "Registro agregado correctamente";
                    ModelState.Clear();
                    Item = (T)Activator.CreateInstance(typeof(T));
                }
                else
                {
                    Mensaje = manager.Error;
                }
            }
            catch (Exception ex)
            {
                Mensaje = ex.Message;
            }
            return Page();
        }


        private IActionResult Editar()
        {
            Exito = false;
            try
            {
                var r = manager.Actualizar(Item);
                if (r != null)
                {
                    Exito = true;
                    Mensaje = "Registro actualizado correctamente";
                    return Page();
                }
                else
                {
                    Mensaje = manager.Error;
                    return Page();
                }
            }
            catch (Exception ex)
            {
                Mensaje = ex.Message;
                return Page();
            }
        }


        private IActionResult Eliminar()
        {
            Exito = false;
            try
            {
                var r = manager.Eliminar(Item.Id);
                if (r)
                {
                    Exito = true;
                    Mensaje = "Registro eliminado correctamente";
                    return Page();
                }
                else
                {
                    Mensaje = manager.Error;
                    return Page();
                }
            }
            catch (Exception)
            {
                Mensaje = "Error, intenta mas tarde...";
                return Page();
            }
        }

    }
}