﻿using IoTCloud2021.COMMON.Entidades;
using IoTCloud2021.COMMON.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoTCloud2021.Web.Models
{
    [Authorize]
    [BindProperties]
    public abstract class GenericModelExaminar<T> : PageModel where T : BaseDTO
    {
        readonly IGenericManager<T> manager;

        public T Item { get; set; }
        public GenericModelExaminar(IGenericManager<T> manager)
        {
            this.manager = manager;
            Item = (T)Activator.CreateInstance(typeof(T));
        }
        public virtual void OnGet(string id)
        {
            Item = manager.BuscarPorId(id);
        }
    }
}
