﻿using IoTCloud2021.COMMON.Entidades;
using IoTCloud2021.COMMON.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoTCloud2021.Web.Models
{
    [Authorize]
    [BindProperties]
    public abstract class GenericModelAgregar<T> : PageModel where T : BaseDTO
    {
        readonly IGenericManager<T> manager;
        public bool Exito { get; set; }
        public bool EsError { get; set; }
        public string Mensaje { get; set; }
        public T Item { get; set; }

        public GenericModelAgregar(IGenericManager<T> manager)
        {
            this.manager = manager;
            Item = (T)Activator.CreateInstance(typeof(T));
        }

        public virtual IActionResult OnPost()
        {
            Exito = false;
            try
            {

                var r = manager.Insertar(Item);
                if (r != null)
                {
                    Exito = true;
                    Mensaje = "Registro agregado correctamente";
                    ModelState.Clear();
                    Item = (T)Activator.CreateInstance(typeof(T));
                }
                else
                {
                    Mensaje = manager.Error;
                }
            }
            catch (Exception ex)
            {
                Mensaje = ex.Message;
            }
            return Page();
        }
    }
}
