﻿using IoTCloud2021.COMMON.Entidades;
using IoTCloud2021.COMMON.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoTCloud2021.Web.Models
{
    [Authorize]
    [BindProperties]
    public abstract class GenericModelIndex<T> : PageModel where T : BaseDTO
    {
        public List<T> Items { get; set; }
        public bool EsError { get; set; }
        public string Mensaje { get; set; }

        internal readonly IGenericManager<T> genericManager;
        public GenericModelIndex(IGenericManager<T> genericManager)
        {
            this.genericManager = genericManager;
        }

    }
}
