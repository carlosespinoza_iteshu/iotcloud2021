﻿using IoTCloud2021.COMMON.Entidades;
using IoTCloud2021.COMMON.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoTCloud2021.Web.Models
{
    [Authorize]
    [BindProperties]
    public abstract class GenericModelEditar<T> : PageModel where T : BaseDTO
    {
        internal readonly IGenericManager<T> manager;
        [BindProperty]
        public bool EsError { get; set; }
        [BindProperty]
        public string Mensaje { get; set; }
        public bool Exito { get; set; }
        [BindProperty]
        public T Item { get; set; }
        public GenericModelEditar(IGenericManager<T> manager)
        {
            this.manager = manager;
            Item = (T)Activator.CreateInstance(typeof(T));
        }
        public virtual void OnGet(string id)
        {
            Item = manager.BuscarPorId(id);
        }
        public virtual IActionResult OnPost()
        {
            Exito = false;
            try
            {
                var r = manager.Actualizar(Item);
                if (r != null)
                {
                    Exito = true;
                    Mensaje = "Registro actualizado correctamente";
                    return Page();
                }
                else
                {
                    Mensaje = manager.Error;
                    return Page();
                }
            }
            catch (Exception ex)
            {
                Mensaje = ex.Message;
                return Page();
            }
        }
    }
}
