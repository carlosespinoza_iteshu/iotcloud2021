﻿using IoTCloud2021.BIZ;
using IoTCloud2021.COMMON.Entidades;
using IoTCloud2021.COMMON.Interfaces;
using IoTCloud2021.COMMON.Modelos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoTCloud2021.Web.api
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : GenericAPIController<Usuario>
    {
        readonly IUsuarioManager managerUsuario;
        public UsuarioController() : base(FabricManager.UsuarioManager)
        {
            managerUsuario = this.manager as IUsuarioManager;
        }
        [HttpPost("CrearCuenta")]
        public ActionResult<string> CrearCuenta([FromBody] NuevaCuentaModel model)
        {
            try
            {
                if (managerUsuario.CrearCuenta(model, out string r))
                {
                    return Ok(r);
                }
                else
                {
                    return BadRequest(manager.Error);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpPost("Login")]
        public ActionResult<Usuario> Login([FromForm] LoginModel model)
        {
            try
            {
                var r= managerUsuario.Login(model);
                if (r != null)
                {
                    return Ok(r);
                }
                else
                {
                    return BadRequest(manager.Error);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
