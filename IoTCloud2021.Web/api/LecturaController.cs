﻿using IoTCloud2021.BIZ;
using IoTCloud2021.COMMON.Entidades;
using IoTCloud2021.COMMON.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using IoTCloud2021.COMMON.Modelos;

namespace IoTCloud2021.Web.api
{
    [Route("api/[controller]")]
    [ApiController]
    public class LecturaController : GenericAPIController<Lectura>
    {
        public LecturaController() : base(FabricManager.LecturaManager)
        {
        }
        [AllowAnonymous]
        [HttpPost("CargarDatos")]
        public ActionResult<DatosParaCargar> CargarDatos([FromBody] DatosParaCargar datos)
        {
            Usuario u = FabricManager.UsuarioManager.Login(new LoginModel()
            {
                Email = datos.Email,
                Password = datos.Password
            });
            if (u != null)
            {
                DatosParaCargar errores = new DatosParaCargar();
                errores.Email = datos.Email;
                errores.Password = datos.Password;
                errores.Estados = new List<EstadoParaCargar>();
                errores.Lecturas = new List<LecturaParaCargar>();
                ILecturaManager lecturaManager = FabricManager.LecturaManager;
                if (datos.Lecturas.Count > 0)
                {
                    var sensores = FabricManager.SensorManager.SensoresDeUsuario(u.Id);
                    foreach (var lectura in datos.Lecturas)
                    {
                        Sensor s = sensores.FirstOrDefault(x => x.Id == lectura.IdSensor);
                        if (s != null)
                        {
                            var r=lecturaManager.Insertar(new Lectura()
                            {
                                IdSensor = s.Id,
                                FechaHora = lectura.FechaHora == null ? DateTime.Now : lectura.FechaHora,
                                IdUsuario = u.Id,
                                IdProyecto = s.IdProyecto,
                                Valor = lectura.Valor
                            });
                            if (r == null)
                            {
                                errores.Lecturas.Add(lectura);
                            }
                        }
                        else
                        {
                            errores.Lecturas.Add(lectura);
                        }
                    }
                }
                IEstadoActuadorManager estadoActuadorManager = FabricManager.EstadoActuadorManager;
                if (datos.Estados.Count > 0)
                {
                    var actuadores = FabricManager.ActuadorManager.ActuadoresDeUsuario(u.Id);
                    foreach (var estado in datos.Estados)
                    {
                        Actuador a = actuadores.FirstOrDefault(x => x.Id == estado.IdActuador);
                        if (a != null)
                        {
                            var r = estadoActuadorManager.Insertar(new EstadoActuador()
                            {
                                Estado=estado.Estado,
                                FechaHora= estado.FechaHora == null ? DateTime.Now : estado.FechaHora,
                                IdActuador=a.Id,
                                IdProyecto=a.IdProyecto,
                                IdUsuario=a.IdUsuario
                            });
                            if (r == null)
                            {
                                errores.Estados.Add(estado);
                            }
                        }
                        else
                        {
                            errores.Estados.Add(estado);
                        }
                    }
                }
                return Ok(errores);
            }
            else
            {
                return BadRequest("Usuario y/o contraseña incorrecta");
            }
        }
    }
}
