﻿using IoTCloud2021.COMMON.Entidades;
using IoTCloud2021.COMMON.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoTCloud2021.Web.api
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public abstract class GenericAPIController<T> : ControllerBase where T : BaseDTO
    {

        protected IGenericManager<T> manager;
        public GenericAPIController(IGenericManager<T> manager)
        {
            this.manager = manager;
        }

        [HttpGet("Total")]
        public ActionResult<int> Total()
        {
            return manager.TotalDeElementos;
        }

        [HttpGet("ObtenerPagina")]
        public ActionResult<IEnumerable<T>> ObtenerPagina(int apartirDeRegistroNum, int registrosAObtener, bool ascendente = true)
        {
            try
            {
                var r = manager.ObtenerTodosPaginado(apartirDeRegistroNum, registrosAObtener, ascendente);
                if (r != null)
                {
                    return Ok(r);
                }
                else
                {
                    return BadRequest(manager.Error);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpGet]

        public ActionResult<IEnumerable<T>> Get()

        {
            try
            {
                var r = manager.ObtenerTodos;

                if (r != null)
                {
                    return Ok(r);
                }
                else
                {
                    return BadRequest(manager.Error);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // GET api/<GenericApiController>/5
        [HttpGet("{id}")]
        public ActionResult<T> Get(string id)
        {
            try
            {
                var r = manager.BuscarPorId(id);
                if (r != null)
                {
                    return Ok(r);
                }
                else
                {
                    return BadRequest(manager.Error);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // POST api/<GenericApiController>
        [HttpPost]
        public ActionResult<T> Post([FromBody] T value)
        {
            try
            {
                var r = manager.Insertar(value);
                if (r != null)
                {
                    return Ok(r);
                }
                else
                {
                    return BadRequest(manager.Error);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // PUT api/<GenericApiController>/5
        [HttpPut]
        public ActionResult<T> Put([FromBody] T value)
        {
            try
            {
                var r = manager.Actualizar(value);
                if (r != null)
                {
                    return Ok(r);
                }
                else
                {
                    return BadRequest(manager.Error);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        // DELETE api/<GenericApiController>/5
        [HttpDelete("{id}")]
        public ActionResult<bool> Delete(string id)
        {
            try
            {
                var r = manager.Eliminar(id);
                if (r)
                {
                    return Ok(r);
                }
                else
                {
                    return BadRequest(manager.Error);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("AgregaVarias")]
        public ActionResult<IEnumerable<T>> AgregaVarias([FromBody] IEnumerable<T> entidades)
        {
            try
            {
                var r = manager.AgregaVarias(entidades);
                return Ok(r);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("EliminaVarias")]
        public ActionResult<IEnumerable<T>> EliminaVarias([FromBody] IEnumerable<string> ids)
        {
            try
            {
                var r = manager.EliminaVarias(ids);
                return Ok(r);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
