using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IoTCloud2021.BIZ;
using IoTCloud2021.COMMON;
using IoTCloud2021.COMMON.Entidades;
using IoTCloud2021.COMMON.Interfaces;
using IoTCloud2021.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace IoTCloud2021.Web.Pages.Lecturas
{
    public class IndexModel : GenericModelIndex<Lectura> 
    {
        public Sensor Sensor { get; set; }
        public Proyecto Proyecto { get; set; }
        public IndexModel() : base(FabricManager.LecturaManager)
        {
        }

        public void OnGet(string id)
        {
            Parametros.IdTempSensor = id;
            Sensor = FabricManager.SensorManager.BuscarPorId(id);
            Proyecto = FabricManager.ProyectoManager.BuscarPorId(Sensor.IdProyecto);
            Items = FabricManager.LecturaManager.LecturasDeSensor(id).ToList();
        }
    }
}
