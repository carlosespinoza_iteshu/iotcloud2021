using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IoTCloud2021.BIZ;
using IoTCloud2021.COMMON.Entidades;
using IoTCloud2021.COMMON.Interfaces;
using IoTCloud2021.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace IoTCloud2021.Web.Pages
{
    public class PanelProyectosModel : GenericModelCRUD<Proyecto>
    {
        IProyectoManager proyectoManager;
        public PanelProyectosModel() : base(FabricManager.ProyectoManager)
        {
        }
        public override void OnGet(string id)
        {
            proyectoManager = manager as IProyectoManager;
            Items = proyectoManager.ProyectosDeUsuario(id);
            Item = new Proyecto();
        }
    }
}
