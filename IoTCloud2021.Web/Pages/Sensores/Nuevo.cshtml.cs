using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IoTCloud2021.BIZ;
using IoTCloud2021.COMMON;
using IoTCloud2021.COMMON.Entidades;
using IoTCloud2021.COMMON.Interfaces;
using IoTCloud2021.Tools;
using IoTCloud2021.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace IoTCloud2021.Web.Pages.Sensores
{
    public class NuevoModel : GenericModelAgregar<Sensor>
    {
        public string IdProyecto { get; set; }
        public NuevoModel() : base(FabricManager.SensorManager)
        {
        }
        public override IActionResult OnPost()
        {
            Item.IdProyecto = IdProyecto;
            Item.IdUsuario = Parametros.UsuarioLogeado.Id;
            Item.TopicoMQTT = Herramientas.GenerarTopicoMQTT();
            var r = base.OnPost();
            if (!EsError)
            {
                return RedirectToPage("/Sensores/Index", new { id = Parametros.IdTempProyecto });
            }
            else
            {
                return r;
            }
        }
        public void OnGet(string id)
        {
            IdProyecto = id;
        }
        
    }
}
