using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IoTCloud2021.BIZ;
using IoTCloud2021.COMMON;
using IoTCloud2021.COMMON.Entidades;
using IoTCloud2021.COMMON.Interfaces;
using IoTCloud2021.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace IoTCloud2021.Web.Pages.Sensores
{
    public class EliminarModel : GenericModelEliminar<Sensor>
    {
        public EliminarModel() : base(FabricManager.SensorManager)
        {
        }
        public override void OnGet(string id)
        {
            base.OnGet(id);
        }
        public override IActionResult OnPost()
        {
            var r= base.OnPost();
            if (!EsError)
            {
                return RedirectToPage("/Sensores/Index", new { id = Parametros.IdTempProyecto });
            }
            else
            {
                return r;
            }
        }
    }
}
