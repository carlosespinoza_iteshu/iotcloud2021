using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IoTCloud2021.BIZ;
using IoTCloud2021.COMMON;
using IoTCloud2021.COMMON.Entidades;
using IoTCloud2021.COMMON.Interfaces;
using IoTCloud2021.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace IoTCloud2021.Web.Pages.Sensores
{
    public class Index : GenericModelExaminar<Proyecto>
    {
        public List<Sensor> Sensores { get; set; }
        public Proyecto Proyecto { get; set; }
        public Index() : base(FabricManager.ProyectoManager)
        {
        }

        public override void OnGet(string id)
        {
            Parametros.IdTempProyecto = id;
            Proyecto = FabricManager.ProyectoManager.BuscarPorId(id);
            Sensores = FabricManager.SensorManager.SensoresDeProyecto(id).ToList();
            base.OnGet(id);
        }
    }
}
