using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IoTCloud2021.BIZ;
using IoTCloud2021.COMMON;
using IoTCloud2021.COMMON.Entidades;
using IoTCloud2021.COMMON.Interfaces;
using IoTCloud2021.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace IoTCloud2021.Web.Pages.Proyectos
{
    [Authorize]
    public class Index : GenericModelIndex<Proyecto>
    {
        public Index() : base(FabricManager.ProyectoManager)
        {
        }
        
        public void OnGet()
        {
            Items = FabricManager.ProyectoManager.ProyectosDeUsuario(Parametros.UsuarioLogeado.Id).ToList();
        }
    }
}
