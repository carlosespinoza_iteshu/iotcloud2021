using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IoTCloud2021.BIZ;
using IoTCloud2021.COMMON;
using IoTCloud2021.COMMON.Entidades;
using IoTCloud2021.COMMON.Interfaces;
using IoTCloud2021.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace IoTCloud2021.Web.Pages.Proyectos
{
    public class EliminarModel : GenericModelEliminar<Proyecto>
    {
        public EliminarModel() : base(FabricManager.ProyectoManager)
        {
        }
        public override IActionResult OnPost()
        {
            var r= base.OnPost();
            if (!EsError)
            {
                return RedirectToPage("/Proyectos/Index");
            }
            else
            {
                return r;
            }
        }
    }
}
