using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IoTCloud2021.BIZ;
using IoTCloud2021.COMMON.Interfaces;
using IoTCloud2021.COMMON.Modelos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace IoTCloud2021.Web.Pages
{
    public class CreaCuentaModel : PageModel
    {
        [BindProperty]
        public NuevaCuentaModel nuevaCuentaModel { get; set; }
        [BindProperty]
        public string Texto { get; set; }
        [BindProperty]
        public bool Resultado { get; set; }

        public void OnGet()
        {
            Texto = "";
        }
        public async Task<IActionResult> OnPostAsync()
        {
            Resultado = FabricManager.UsuarioManager.CrearCuenta(nuevaCuentaModel,out string texto);
            Texto = texto;
            return Page();
        }
    }
}
