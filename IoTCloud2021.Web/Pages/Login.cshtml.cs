using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using IoTCloud2021.BIZ;
using IoTCloud2021.COMMON;
using IoTCloud2021.COMMON.Entidades;
using IoTCloud2021.COMMON.Modelos;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace IoTCloud2021.Web.Pages
{
    public class Login : PageModel
    {
        [BindProperty]
        public string Texto { get; set; }
        [BindProperty]
        public bool Resultado { get; set; }
        [BindProperty]
        public LoginModel loginModel { get; set; }
        public void OnGet()
        {
            Texto = "";
        }

        public async Task<IActionResult> OnPostAsync()
        {
            Usuario u = FabricManager.UsuarioManager.Login(loginModel);
            if (u != null)
            {
                Parametros.UsuarioLogeado = u;
                List<Claim> claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Name,u.Nombre + " " + u.Apellidos),
                        new Claim(ClaimTypes.Email,u.Email)
                    };
                ClaimsIdentity identity = new ClaimsIdentity(claims, "cookie");
                ClaimsPrincipal principal = new ClaimsPrincipal(identity);
                await HttpContext.SignInAsync(scheme: "Seguridad", principal: principal, properties: new AuthenticationProperties
                {
                    ExpiresUtc = DateTime.UtcNow.AddMinutes(30)
                });
                Texto = $"Bienvenido {u.Nombre}";
                Resultado = true;
                return RedirectToPage("Proyectos/Index");
            }
            else
            {
                Texto = "Usuario y/o contraseņa incorrecta...";
                Resultado = false;
                return Page();

            }
            
        }
    }
}
