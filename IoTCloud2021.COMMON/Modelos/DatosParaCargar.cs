﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoTCloud2021.COMMON.Modelos
{
    public class DatosParaCargar
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public List<LecturaParaCargar> Lecturas { get; set; }
        public List<EstadoParaCargar> Estados { get; set; }
    }
    public class LecturaParaCargar
    {
        public DateTime FechaHora { get; set; }
        public string IdSensor { get; set; }
        public float Valor { get; set; }
    }

    public class EstadoParaCargar
    {
        public DateTime FechaHora { get; set; }
        public string IdActuador { get; set; }
        public string Estado { get; set; }
    }
}
