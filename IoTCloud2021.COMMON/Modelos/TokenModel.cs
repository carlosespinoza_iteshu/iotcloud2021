﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoTCloud2021.COMMON.Modelos
{
    public class TokenModel
    {
        public string Token { get; set; }
        public DateTime FechaHoraCreacion { get; set; }
        public DateTime FechaHoraTermino { get; set; }
        public string IdUsuario { get; set; }
        public string Nombre { get; set; }
        public string UrlFoto { get; set; }
        public string Email { get; set; }
    }
}
