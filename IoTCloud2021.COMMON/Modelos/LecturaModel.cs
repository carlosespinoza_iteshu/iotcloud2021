﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoTCloud2021.COMMON.Modelos
{
    public class LecturaModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Sensor { get; set; }
        public float Valor { get; set; }
    }
}
