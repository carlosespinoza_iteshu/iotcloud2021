﻿using IoTCloud2021.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoTCloud2021.COMMON.Interfaces
{
    public interface IProyectoManager:IGenericManager<Proyecto>
    {
        /// <summary>
        /// Elimina todos los proyectos de un usuario
        /// </summary>
        /// <param name="idUsuario">Id del Usuario</param>
        void EliminarProyectosDeUsuario(string idUsuario);
        IEnumerable<Proyecto> ProyectosDeUsuario(string id);
    }

}
