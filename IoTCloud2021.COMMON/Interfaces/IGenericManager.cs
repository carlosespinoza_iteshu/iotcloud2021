﻿using IoTCloud2021.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoTCloud2021.COMMON.Interfaces
{
    public interface IGenericManager<T> where T:BaseDTO
    {
        /// <summary>
        /// Proporciona el error relacionado después de alguna operación
        /// </summary>
        string Error { get; }
        /// <summary>
        /// Inserta una entidad en la tabla
        /// </summary>
        /// <param name="entidad">Entidad a Insertar</param>
        /// <returns>Confirmación de Inserción</returns>
        T Insertar(T entidad);
        /// <summary>
        /// Obtiene todos los registros de la tabla
        /// </summary>
        IEnumerable<T> ObtenerTodos { get; }
        /// <summary>
        /// Obtiene la lista de elementos paginada
        /// </summary>
        /// <param name="apartirDeRegistroNum">Numero de Registro desde el cual empieza la pagina</param>
        /// <param name="registrosAObtener">Total de registros a obtener</param>
        /// <param name="ascendente">Indica si la ordenación será Ascendente o Descendente según su FechaHora</param>
        /// <returns></returns>
        IEnumerable<T> ObtenerTodosPaginado(int apartirDeRegistroNum, int registrosAObtener, bool ascendente = true);

        /// <summary>
        /// Actualiza un registro en la tabla en base a la propiedad Id
        /// </summary>
        /// <param name="entidad">Entidad ya modificada, el Id debe existir en la tabla</param>
        /// <returns>Confirmación de actualización</returns>
        T Actualizar(T entidad);
        /// <summary>
        /// Elimina una entidad en base al Id proporcionado
        /// </summary>
        /// <param name="id">Id de la entidad a eliminar</param>
        /// <returns>Confirmación de eliminación</returns>
        bool Eliminar(string id);
        /// <summary>
        /// Obtiene un elemento de acuerdo a su Id
        /// </summary>
        /// <param name="id">Id en formato cadena del producto a obtener</param>
        /// <returns>Objeto relativo al Id proporcionado</returns>
        T BuscarPorId(string id);
        /// <summary>
        /// Agrega múltiples entidades en la Base de datos
        /// </summary>
        /// <param name="entidades">Entidades a agregar</param>
        /// <returns>Entidades no Agregadas</returns>
        IEnumerable<T> AgregaVarias(IEnumerable<T> entidades);
        /// <summary>
        /// Elimina multiples entidaes
        /// </summary>
        /// <param name="ids">Id's de entidades a eliminar</param>
        /// <returns>Id's no eliminadas</returns>
        IEnumerable<string> EliminaVarias(IEnumerable<string> ids);

        /// <summary>
        /// Obtiene el total de elementos de la colección
        /// </summary>
        int TotalDeElementos { get; }
    }
}
