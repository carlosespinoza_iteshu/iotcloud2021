﻿using IoTCloud2021.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoTCloud2021.COMMON.Interfaces
{
    public interface IActuadorManager:IGenericManager<Actuador>
    {
        void EliminarActuadoresDeProyecto(string idProyecto);
        void EliminarActuadoresDeUsuario(string idUsuario);
        IEnumerable<Actuador> ActuadoresDeUsuario(string idUsuario);
    }
}
