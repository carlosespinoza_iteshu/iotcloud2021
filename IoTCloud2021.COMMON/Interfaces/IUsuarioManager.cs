﻿using IoTCloud2021.COMMON.Entidades;
using IoTCloud2021.COMMON.Modelos;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoTCloud2021.COMMON.Interfaces
{
    public interface IUsuarioManager:IGenericManager<Usuario>
    {
        Usuario Login(LoginModel model);
        bool CrearCuenta(NuevaCuentaModel model,out string resultado);
    }
}
