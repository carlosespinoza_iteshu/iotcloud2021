﻿using IoTCloud2021.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace IoTCloud2021.COMMON.Interfaces
{
    public interface IGenericRepository<T> where T:BaseDTO
    {
        /// <summary>
        /// Proporciona información sobre el error ocurrido en alguna de las operaciones
        /// </summary>
        string Error { get; }
        /// <summary>
        /// Inserta una entidad en la base de datos
        /// </summary>
        /// <param name="entidad">Entidad a insertar sin Id</param>
        /// <returns>Entidad completa</returns>
        T Create(T entidad);
        /// <summary>
        /// Regresa todos los registros de la tabla
        /// </summary>
        IEnumerable<T> Read { get; }
        /// <summary>
        /// Actualiza una entidad en la base de datos
        /// </summary>
        /// <param name="entidad">Entidad a actualizar en base a su Id</param>
        /// <returns>Entidad actualizada completa</returns>
        T Update(T entidad);
        /// <summary>
        /// Elimina una entidad en base al Id proporcionado
        /// </summary>
        /// <param name="id">Id de la entidad a eliminar</param>
        /// <returns>La confirmación de que fue eliminada la entidad</returns>
        bool Delete(string id);
        /// <summary>
        /// Ejecuta una consulta sobre la tabla, regresando todos los campos de la misma 
        /// </summary>
        /// <param name="predicado">Lamda de la consulta</param>
        /// <returns>Conjunto de entidad que coinciden con la consulta</returns>
        IEnumerable<T> Query(Expression<Func<T, bool>> predicado);
        /// <summary>
        /// Busca una entidad por su Id
        /// </summary>
        /// <param name="id">Id de la entidad a buscar</param>
        /// <returns>Entidad que coincide con el Id</returns>
        T SearchById(string id);
        /// <summary>
        /// Agrega múltiples entidades en la Base de datos
        /// </summary>
        /// <param name="entidades">Entidades a agregar</param>
        /// <returns>Entidades no Agregadas</returns>
        IEnumerable<T> AddMultiple(IEnumerable<T> entidades);
        /// <summary>
        /// Elimina multiples entidaes
        /// </summary>
        /// <param name="ids">Id's de entidades a eliminar</param>
        /// <returns>Id's no eliminadas</returns>
        IEnumerable<string> DeleteMultiple(IEnumerable<string> ids);
        /// <summary>
        /// Obtiene la lista de elementos paginada
        /// </summary>
        /// <param name="apartirDeRegistroNum">Numero de Registro desde el cual empieza la pagina</param>
        /// <param name="registrosAObtener">Total de registros a obtener</param>
        /// <param name="ascendente">Indica si la ordenación será Ascendente o Descendente según su FechaHora</param>
        /// <returns></returns>
        IEnumerable<T> ReadPage(int apartirDeRegistroNum, int registrosAObtener,  bool ascendente = true);
        /// <summary>
        /// Obtiene el numero de registros de la colección
        /// </summary>
        int Count { get; }
    }
}
