﻿using IoTCloud2021.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoTCloud2021.COMMON.Interfaces
{
    public interface ISensorManager:IGenericManager<Sensor>
    {
        void EliminarSensoresDeProyecto(string idProyecto);
        void EliminarSensoresDeUsuario(string idUsuario);
        IEnumerable<Sensor> SensoresDeProyecto(string idProyecto);
        IEnumerable<Sensor> SensoresDeUsuario(string idUsuario);
    }
}
