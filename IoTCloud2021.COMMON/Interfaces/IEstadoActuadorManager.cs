﻿using IoTCloud2021.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoTCloud2021.COMMON.Interfaces
{
    public interface IEstadoActuadorManager:IGenericManager<EstadoActuador>
    {
        void EliminarEstadoDeActuadorDeProyecto(string idProyecto);
        void EliminarEstadoDeActuadorDeUsuario(string idUsuario);
        void EliminarEstadosDeActuador(string idActuador);

    }
}
