﻿using IoTCloud2021.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoTCloud2021.COMMON.Interfaces
{
    public interface ILecturaManager:IGenericManager<Lectura>
    {
        void EliminarLecturasDeProyecto(string idProyecto);
        void EliminarLecturasDeUsuario(string idUsuario);
        void EliminarLecturasDeSensor(string idSensor);
        IEnumerable<Lectura> LecturasDeSensor(string idSensor);
    }
}
