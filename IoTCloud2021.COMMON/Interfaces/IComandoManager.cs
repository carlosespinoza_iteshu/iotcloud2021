﻿using IoTCloud2021.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoTCloud2021.COMMON.Interfaces
{
    public interface IComandoManager:IGenericManager<Comando>
    {
        void EliminarComandoDeProyecto(string idProyecto);
        void EliminarComandoDeUsuario(string idUsuario);
    }
}
