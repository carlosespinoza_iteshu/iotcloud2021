﻿using FluentValidation;
using IoTCloud2021.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoTCloud2021.COMMON.Validadores
{
    public class SensorValidator:GenericValidator<Sensor>
    {
        public SensorValidator()
        {
            RuleFor(s => s.IdProyecto).NotEmpty();
            RuleFor(s => s.IdUsuario).NotEmpty();
            RuleFor(s => s.Nombre).NotEmpty();
            RuleFor(s => s.TopicoMQTT).NotEmpty().MinimumLength(5);
            RuleFor(s => s.UnidadDeMedida).NotEmpty();
        }
    }
}
