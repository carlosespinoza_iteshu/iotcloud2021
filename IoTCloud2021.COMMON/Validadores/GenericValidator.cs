﻿using FluentValidation;
using IoTCloud2021.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace IoTCloud2021.COMMON.Validadores
{
    public abstract class GenericValidator<T>:AbstractValidator<T> where T:BaseDTO
    {
        public GenericValidator()
        {
            RuleFor(b => b.Id).NotEmpty();
            RuleFor(b => b.FechaHora).NotEmpty();
            ValidatorOptions.Global.LanguageManager.Culture = new CultureInfo("es");
        }
    }
}
