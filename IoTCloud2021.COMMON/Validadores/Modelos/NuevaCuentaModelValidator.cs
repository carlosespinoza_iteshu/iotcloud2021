﻿using FluentValidation;
using IoTCloud2021.COMMON.Modelos;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace IoTCloud2021.COMMON.Validadores.Modelos
{
    public class NuevaCuentaModelValidator:AbstractValidator<NuevaCuentaModel>
    {
        public NuevaCuentaModelValidator()
        {
            ValidatorOptions.Global.LanguageManager.Culture = new CultureInfo("es");
            RuleFor(u => u.Apellido).NotEmpty().MinimumLength(5).MaximumLength(100);
            RuleFor(u => u.Nombre).NotEmpty().MinimumLength(5).MaximumLength(100);
            RuleFor(u => u.Email).NotEmpty().EmailAddress();
            RuleFor(u => u.Password1).NotEmpty().MinimumLength(10);
            RuleFor(u => u.Password2).NotEmpty().MinimumLength(10);
            RuleFor(u => u.Password1).Equal(p => p.Password2).WithMessage("Las contraseñas deben ser iguales");
        }
    }
}
