﻿using FluentValidation;
using IoTCloud2021.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoTCloud2021.COMMON.Validadores
{
    public class ActuadorValidator: GenericValidator<Actuador>
    {
        public ActuadorValidator()
        {
            RuleFor(a => a.IdProyecto).NotEmpty();
            RuleFor(a => a.IdUsuario).NotEmpty();
            RuleFor(a => a.Nombre).NotEmpty();
            RuleFor(a => a.TopicoMQTT).NotEmpty();
            RuleFor(a => a.UnidadDeMedida).NotEmpty();

        }
    }
}
