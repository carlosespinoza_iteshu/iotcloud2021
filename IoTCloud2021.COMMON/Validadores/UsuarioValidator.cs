﻿using FluentValidation;
using IoTCloud2021.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoTCloud2021.COMMON.Validadores
{
    public class UsuarioValidator:GenericValidator<Usuario>
    {
        public UsuarioValidator()
        {
            RuleFor(u => u.Apellidos).NotEmpty().MinimumLength(5).MaximumLength(100);
            RuleFor(u => u.Nombre).NotEmpty().MinimumLength(5).MaximumLength(100);
            RuleFor(u => u.TopicoMQTT).NotEmpty().MinimumLength(5).MaximumLength(100);
            RuleFor(u => u.Email).NotEmpty().EmailAddress();
            RuleFor(u => u.Password).NotEmpty();

        }
    }
}
