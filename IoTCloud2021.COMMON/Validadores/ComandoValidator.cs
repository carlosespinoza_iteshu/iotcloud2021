﻿using FluentValidation;
using IoTCloud2021.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoTCloud2021.COMMON.Validadores
{
    public class ComandoValidator:GenericValidator<Comando>
    {
        public ComandoValidator()
        {
            RuleFor(c => c.Texto).NotEmpty();
            RuleFor(c => c.IdProyecto).NotEmpty();
            RuleFor(c => c.IdUsuario).NotEmpty();
            RuleFor(c => c.Nombre).NotEmpty();

        }
    }
}
