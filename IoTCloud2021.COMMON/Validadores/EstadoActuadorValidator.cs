﻿using FluentValidation;
using IoTCloud2021.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoTCloud2021.COMMON.Validadores
{
    public class EstadoActuadorValidator:GenericValidator<EstadoActuador>
    {
        public EstadoActuadorValidator()
        {
            RuleFor(e => e.Estado).NotEmpty();
            RuleFor(e => e.IdActuador).NotEmpty();
            RuleFor(e => e.IdProyecto).NotEmpty();
            RuleFor(e => e.IdUsuario).NotEmpty();
        }
    }
}
