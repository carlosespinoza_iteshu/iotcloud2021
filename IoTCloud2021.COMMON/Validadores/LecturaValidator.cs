﻿using FluentValidation;
using IoTCloud2021.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoTCloud2021.COMMON.Validadores
{
    public class LecturaValidator:GenericValidator<Lectura>
    {
        public LecturaValidator()
        {
            RuleFor(l => l.IdProyecto).NotEmpty();
            RuleFor(l => l.IdSensor).NotEmpty();
            RuleFor(l => l.IdUsuario).NotEmpty();
            RuleFor(l => l.Valor).NotEmpty();
        }
    }
}
