﻿using FluentValidation;
using IoTCloud2021.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoTCloud2021.COMMON.Validadores
{
    public class ProyectoValidator:GenericValidator<Proyecto>
    {
        public ProyectoValidator()
        {
            RuleFor(p => p.IdUsuario).NotEmpty();
            RuleFor(p => p.Nombre).NotEmpty().MinimumLength(5);
            RuleFor(p => p.TopicoMQTT).NotEmpty().MinimumLength(5);
        }
    }
}
