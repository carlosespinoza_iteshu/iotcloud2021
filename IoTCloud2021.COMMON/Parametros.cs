﻿using IoTCloud2021.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoTCloud2021.COMMON
{
    public static class Parametros
    {
        public static Usuario UsuarioLogeado { get; set; }
        public static string IdTempProyecto { get; set; }
        public static string IdTempSensor { get; set; }

        public static string NombreApp
        {
            get
            {
                return App.NombreApp;
            }
        }
        
    }
}
