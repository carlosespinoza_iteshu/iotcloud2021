﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoTCloud2021.COMMON.Enumeraciones
{

    public enum EnumTipoOperacion
    {
        Listar,
        Insertar,
        Actualizar,
        Eliminar
    }
}

