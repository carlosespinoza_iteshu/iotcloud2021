﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoTCloud2021.COMMON.Enumeraciones
{
    public enum EnumTipoGrafico
    {
        Lineas,
        Area,
        Barras,
        Dona
    }
}
