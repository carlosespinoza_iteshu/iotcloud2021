﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoTCloud2021.COMMON.Entidades
{
    public class Lectura:BaseDTO
    {
        public string IdUsuario { get; set; }
        public string IdProyecto { get; set; }
        public string IdSensor { get; set; }
        public string Referencia { get; set; }
        public float Valor { get; set; }
    }
}
