﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoTCloud2021.COMMON.Entidades
{
    public class Proyecto:BaseDTO
    {
        public string IdUsuario { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string URLFoto { get; set; }
        public string TopicoMQTT { get; set; }
    }
}
