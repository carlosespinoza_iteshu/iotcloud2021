﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoTCloud2021.COMMON.Entidades
{
    public class Comando:BaseDTO
    {
        public string IdUsuario { get; set; }
        public string IdProyecto { get; set; }
        public string Nombre { get; set; }
        public string Texto { get; set; }
    }
}
