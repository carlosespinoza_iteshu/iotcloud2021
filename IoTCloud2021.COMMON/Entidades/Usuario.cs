﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoTCloud2021.COMMON.Entidades
{
    public class Usuario:BaseDTO
    {
        public string Email { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string Password { get; set; }
        public string URLFoto { get; set; }
        public string TopicoMQTT { get; set; }
    }
}
