﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoTCloud2021.COMMON.Entidades
{
    public class Actuador:BaseDTO
    {
        public string IdUsuario { get; set; }
        public string IdProyecto { get; set; }
        public string Nombre { get; set; }
        public string UnidadDeMedida { get; set; }
        public string ComandoBase { get; set; }
        public string URLFoto { get; set; }
        public string TopicoMQTT { get; set; }

    }
}
